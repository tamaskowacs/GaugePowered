define([
    "atemi/io/jsonrpc",
    "atemi/util/TableSort",
    "atemi/thirdparty/moment",

    "bootstrap/Modal",
    "bootstrap/Tooltip",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/query"

], function(
    jsonrpc,
    TableSort,
    moment,

    Modal,
    Tooltip,

    array,
    event,
    fx,
    dom,
    domClass,
    domConstruct,
    on,
    query
) {
    //--------------------------------------------------------------------------
    var init = function() {
        /* Provide a mechanism for a person that already owns this game to 
           rate the game from the game details interface */
        var rpc = jsonrpc('/jsonrpc');

        query('td.watchlist input').tooltip({
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p>Enable Watchlist notifications to this address.</p>'
        });
        query('td.marketing input').tooltip({
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p>Receive marketing and promotional messaging to this address.</p>'
        });
        query('td.confirmation a.resend').tooltip({
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p>Click here to send another confirmation email.</p>'
        });

        /* Setup the add email handlers */
        var email = dom.byId("email");
        var email_button = query(".add-email");
        var message = "Must be a valid email address";
        email_button.tooltip({
            trigger: 'manual',
            placement: 'right',
            html: false,
            title: function() { return message; }
        });
        on(email_button, 'click', function(evt) {
            event.stop(evt);

            rpc.request({
                method: 'useraccountemail.create',
                params: ['select(id)', {
                    email: email.value
                }]
            }, function(data) {
                email_button.tooltip('hide');
                window.location.reload();

            }, function(error) {
                message  = error.error.message;

                if (message.indexOf("Refusing to email them") != -1) {
                    message = '"' + email.value + '" cannot be added to your account.';

                } else if (message.indexOf("is not a valid e-mail address") != -1) {
                    message = '"' + email.value + '" is not a valid email address.';

                } else {
                    message = "There was a problem adding this email address.";
                }
                email_button.tooltip('show');
            });
        });
        on(email, 'focus', function(evt) {
            email_button.tooltip('hide');
        });

        var table = dom.byId('emails');
        var email_rows = query("tr.email-row", table);
        array.forEach(email_rows, function(tr) {
            var email_id = parseInt(tr.getAttribute("data-email-id"));

            /* Hook up to the watch-list switch */
            var watch_list_checkbox = query('.watchlist input', tr)[0];
            on(watch_list_checkbox, 'change', function(evt) {
                rpc.request({
                    method: 'useraccountemail.update',
                    params: ['id=' + email_id + '&select(id,watch_list_notifications)', {
                        watch_list_notifications: watch_list_checkbox.checked
                    }]
                }, function(data) {
                    watch_list_checkbox.checked = data[0].watch_list_notifications;
                });
            });

            /* Hook up to the marketing switch */
            var marketing_checkbox = query('.marketing input', tr)[0];
            on(marketing_checkbox, 'change', function(evt) {
                rpc.request({
                    method: 'useraccountemail.update',
                    params: ['id=' + email_id + '&select(id,marketing_notifications)', {
                        marketing_notifications: marketing_checkbox.checked
                    }] 
                }, function(data) {
                    marketing_checkbox.checked = data[0].marketing_notifications
                });
            });

            /* Hook up the resend confirmation email link */
            var resend_link = query(".resend", tr)[0];
            if (resend_link) {
                on(resend_link, "click", function(evt) {
                    event.stop(evt);
                    rpc.request({
                        method: 'useraccountemail.resend-confirmation-email',
                        params: ['id=' + email_id + '&select(id)']
                    }, function(data) {
                        query(resend_link).tooltip('destroy');
                        domConstruct.place(
                            domConstruct.create('p', {innerHTML: '(confirmation resent)', className: 'message'}),
                            resend_link,
                            'before'
                        );
                        domConstruct.destroy(resend_link);
                    });
                });
            }

            /* Hook up to the remove buttons */
            var remove_button = query(".remove-email", tr);
            on(remove_button, "click", function(evt) {
                event.stop(evt);
                rpc.request({
                    method: 'useraccountemail.delete',
                    params: ['id=' + email_id + '&select(id)']
                }, function(data) {
                    fx.animateProperty({
                        node: tr,
                        properties: {
                            opacity: {start: 1, end: 0}
                        },
                        onEnd: function() {
                            tr.parentNode.removeChild(tr);

                            if (query('tr.email-row').length < 1) {
                                query('tbody, thead').addClass('hidden');
                            }
                        }
                    }).play();
                });
            });
        });

        /* Hook up to the Data usage opt out */
        var data_usage_opt_out = dom.byId('data-usage-opt-out');
        on(data_usage_opt_out, 'change', function(evt) {
            var id = parseInt(data_usage_opt_out.getAttribute("data-user-attrs-id"));       
            var data_usage_opt_out_value = rpc.date_format(new Date());
            if (!data_usage_opt_out.checked) {
                data_usage_opt_out_value = null;
            }
            rpc.request({
            method: 'useraccountattributes.update',
            params: ['id=' + id + '&select(id,data_usage_opt_out)', {
                    data_usage_opt_out: data_usage_opt_out_value
                }] 
            }, function(data) {
                if (data[0].data_usage_opt_out) {
                    data_usage_opt_out.checked = true;                 
                } else {
                    data_usage_opt_out.checked = false;
                }
            });
        });

        (function() {
            /* Hook up the remove friend buttons. */
            array.forEach(query('tbody tr', 'friend-list'), function(tr) {

                on(query('a.remove-friend', tr), 'click', function(evt) {
                    event.stop(evt);

                    if (!confirm('Removing this friend will restrict your access to their Library.')) {
                        return;
                    }

                    var id = parseInt(tr.getAttribute('data-user-account-friend-id'));
                    rpc.request({
                        method: 'useraccountfriend.delete',
                        params: ['id=' + id + '&select(id)']
                    }, function(data) {
                        fx.animateProperty({
                            node: tr,
                            properties: {
                                opacity: {start: 1, end: 0}
                            },
                            onEnd: function() {
                                domConstruct.destroy(tr);
                            }
                        }).play();
                    }); 
                });
            });
        })();

        (function() {
            /* Hook up the remove tag buttons. */
            array.forEach(query('tbody tr', 'tag-list'), function(tr) {
                on(query('a.remove', tr), 'click', function(evt) {
                    event.stop(evt);

                    var id = parseInt(tr.getAttribute('data-user-account-tag-id'));
                    var count = parseInt(query('td.count', tr)[0].innerHTML);
                    if (count) {
                        if (!confirm('Careful, removing this Tag will untag ' + count + ' games...')) {
                            return;
                        }
                    }

                    rpc.request({
                        method: 'useraccounttag.delete',
                        params: ['id=' + id]
                    }, function(data) {
                        fx.animateProperty({
                            node: tr,
                            properties: {
                                opacity: {start: 1, end: 0}
                            },
                            onEnd: function() {
                                domConstruct.destroy(tr);
                            }
                        }).play();
                    });
                });
            });

            /* Hook up the edit buttons */
            var modal = dom.byId('edit-tag-modal');
            query(modal).modal({show: false});

            array.forEach(query('tbody tr', 'tag-list'), function(tr) {
                on(query('a.edit', tr), 'click', function(evt) {
                    event.stop(evt);

                    modal.setAttribute(
                        'data-user-account-tag-id',
                        tr.getAttribute('data-user-account-tag-id')
                    );
                    query('input.name', modal)[0].value = query('td.name', tr)[0].innerHTML;
                    query('input.group', modal)[0].value = query('td.group', tr)[0].innerHTML;
                    query(modal).modal('show');                     
                });
            }); 

            on_save_handler = function(evt) {
                event.stop(evt);

                var id = modal.getAttribute('data-user-account-tag-id');
                var new_tag_name = query('input.name', modal)[0].value;
                    new_tag_name = new_tag_name.replace(/^\s+|\s+$/g, '');
                var new_group_name = query('input.group', modal)[0].value;
                    new_group_name = new_group_name.replace(/^\s+|\s+$/g, '');

                rpc.request({
                    method: 'useraccounttag.update',
                    params: ['id=' + id, {
                        name: new_tag_name,
                        group: new_group_name || null
                    }]
                }, function(data) {
                    query('tr', 'tag-list').some(function(tr) {
                        if (tr.getAttribute('data-user-account-tag-id') == id) {
                            query('td.name', tr)[0].innerHTML = data[0].name;
                            query('td.group', tr)[0].innerHTML = data[0].group;
                            return true;
                        }
                        return false;
                    });
                    query(modal).modal('hide');

                }, function(data) {
                    var field = 'name: ';
                    query('input.name', modal).tooltip('destroy');
                    if (data.error.message.slice(0, field.length) == field) {
                        // There was an error with the name field.
                        query('input.name', modal).tooltip({
                            html: false,
                            placement: 'right',
                            title: data.error.message.slice(field.length, data.error.message.length),
                            trigger: 'manual'
                        });
                        query('input.name', modal).tooltip('show');
                    }

                    field = 'group: ';
                    query('input.group', modal).tooltip('destroy');
                    if (data.error.message.slice(0, field.length) == field) {
                        // There was an error with the group field.
                        query('input.group', modal).tooltip({
                            html: false,
                            placement: 'right',
                            title: data.error.message.slice(field.length, data.error.message.length),
                            trigger: 'manual'
                        });
                        query('input.group', modal).tooltip('show');
                    }
                });
            };
            on(query('button.primary', modal), 'click', on_save_handler);
            on(query('form', modal), 'submit', on_save_handler);

            /* Hook up the modal error tooltips. */
            on(modal, 'hide', function(evt) {
                query('input.name', modal).tooltip('destroy');
                query('input.group', modal).tooltip('destroy');
            });
            on(query('input.name', modal), 'focus', function(evt) {
                query('input.name', modal).tooltip('destroy');
            });
            on(query('input.group', modal), 'focus', function(evt) {
                query('input.group', modal).tooltip('destroy');
            });

            /* Hook up the add tag form */
            (function() { 
                var form = dom.byId('add-tag-form');
                if (!form) {
                    return;
                }
                var input = {
                    name: query('.name', form)[0],
                    group: query('.group', form)[0]
                };
                if (!form) {
                    return;
                }

                on(form, 'submit', function(evt) {
                    event.stop(evt);
    
                    var tag = {
                        name: input.name.value,
                        group: input.group.value
                    };
                    tag.name = tag.name.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                    tag.group = tag.group.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
                    if (!tag.group) {
                        tag.group = null;
                    }

                    if (!tag.name && !tag.group) {
                        // No need submitting an empty form.
                        return;
                    }

                    // Submit the request to the server.
                    rpc.request({
                        method: 'useraccounttag.create',
                        params: ['', tag]
                    }, function(data) {
                        query(input.name).tooltip('destroy');
                        query(input.group).tooltip('destroy');
                        window.location.reload();
                    }, function(data) {
                        var message = data.error.message;

                        var field = 'name: ';
                        query(input.name).tooltip('destroy');
                        if (message.slice(0, field.length) == field) {
                            query(input.name).tooltip({
                                html: 'false',
                                placement: 'top',
                                title: message.slice(field.length, message.length),
                                trigger: 'manual'
                            });
                            query(input.name, modal).tooltip('show');
                        } 

                        field = 'group: ';
                        query(input.group).tooltip('destroy');
                        if (message.slice(0, field.length) == field) {
                            query(input.group).tooltip({
                                html: 'false',
                                placement: 'top',
                                title: message.slice(field.length, message.length),
                                trigger: 'manual'
                            });
                            query(input.group, modal).tooltip('show');
                        }
                    });
                }); 
                on(input.name, 'focus', function(evt) {
                    query(input.name).tooltip('destroy');
                    query(input.group).tooltip('destroy');
                });
                on(input.group, 'focus', function(evt) {
                    query(input.name).tooltip('destroy');
                    query(input.group).tooltip('destroy');
                });
            })();

            /* Hook up the sorting on the tag settings table. */
            new TableSort(
                dom.byId('tag-list'), [0, 0], [
                    TableSort.textContent,
                    TableSort.textContent,
                    TableSort.parseInt
                ]
            );             
        })();

        /* Setup the payment history table. */
        (function() {
            query('td.when', 'payments').forEach(function(td) {
                var m = moment(new Date(td.getAttribute('data-when')));
                td.innerHTML = m.format('MMM D, YYYY') + ' at ' + m.format('h:mm a');
            });

            query('.payment-history .until').forEach(function(p) {
                var m = moment(new Date(p.getAttribute('data-when')));
                query('strong', p)[0].innerHTML = m.format('MMMM D, YYYY') + ' at ' + m.format('h:mm a');
            });

            new TableSort(
                dom.byId('payments'), [0, 0], [
                    TableSort.textContent,
                    function(td) {
                        return new Date(td.getAttribute('data-when'));
                    },
                    TableSort.textContent
                ]
            );
        })();

        /* Setup the currency settings button. */
        (function() {
            var region_select = dom.byId('region');
            on(region_select, 'change', function(evt) {
                var message = query('.pricing-settings .message')[0];
                var id = parseInt(region_select.getAttribute("data-user-attrs-id"));       
                var region_value = region_select.value;
                rpc.request({
                    method: 'useraccountattributes.update',
                    params: ['id=' + id + '&select(id,region)', {
                        region: region_value
                    }] 
                }, function(data) {
                    // Turn off the selected attributes for all of the options.
                    query("option", region_select).forEach(function(element) {
                        element.selected = false;
                    });

                    // Find and "select" the appropriate option
                    var selected_option = query("option[value=" + data[0].region + "]", region_select);
                    if (selected_option) {
                        selected_option[0].selected = true;
                        message.style.opacity = 1;
                        fx.animateProperty({
                            node: message,
                            duration: 2000,
                            properties: {
                                opacity: {start: 1, end: 0}
                            }
                        }).play();
                    } else {
                        // In the case where we can't find the appropriate option
                        // We have to assume that the options changed? Or something
                        // is wrong? So let's reload the page and hope that helps.
                        window.reload();
                    }
                    
                });
            });
        })();
    };
    return init;
});
