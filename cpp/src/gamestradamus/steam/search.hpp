//------------------------------------------------------------------------------
// Define a search engine.
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_STEAM_SEARCH_HPP
#define GAMESTRADAMUS_STEAM_SEARCH_HPP

#include <gamestradamus/steam/game.hpp> 
#include <gamestradamus/steam/account.hpp> 
#include <gamestradamus/steam/accountgame.hpp> 

#include <boost/cstdint.hpp> 
#include <boost/foreach.hpp>
#include <boost/heap/fibonacci_heap.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple.hpp>

#include <iostream>
#include <cstdlib>
#include <limits>
#include <map>
#include <vector>
#include <algorithm>


namespace gamestradamus {
namespace steam {


//------------------------------------------------------------------------------
struct Match {
    Match(double d, uint_fast64_t a, double h) : match_value(d), account_id(a), hours_played(h) { }
    double match_value;
    uint_fast64_t account_id;
    double hours_played;
    bool operator <(const Match rhs) const {
        if (match_value == rhs.match_value) {
            return account_id < rhs.account_id;
        } else {
            return match_value < rhs.match_value;
        }
    }
};

typedef boost::heap::fibonacci_heap<Match> match_heap_t;

//------------------------------------------------------------------------------
class ConsumerTraitBucket {
public:
    //--------------------------------------------------------------------------
    // default ctor
    ConsumerTraitBucket()
        :
            my_min(std::numeric_limits<AccountGame::hours_t>::max()),
            my_max(std::numeric_limits<AccountGame::hours_t>::min())
    {}

    //--------------------------------------------------------------------------
    // copy ctor
    ConsumerTraitBucket(const ConsumerTraitBucket &rhs)
        : my_min(rhs.my_min), my_max(rhs.my_max), my_accounts(rhs.my_accounts)
    {}

    //--------------------------------------------------------------------------
    bool contains(AccountGame::hours_t hours_played) const {
        return my_min <= hours_played && hours_played <= my_max;
    }

    //--------------------------------------------------------------------------
    std::vector<AccountGame>::size_type size() const {
        return my_accounts.size();
    }

    //--------------------------------------------------------------------------
    void push_back(AccountGame ag) {
        my_min = std::min(my_min, ag.hours_played);
        my_max = std::max(my_min, ag.hours_played);
        my_accounts.push_back(ag);
    }

    //--------------------------------------------------------------------------
    void extend(const ConsumerTraitBucket * const rhs) {
        BOOST_FOREACH(AccountGame ag, rhs->my_accounts) {
            push_back(ag);
        }
    }

    //--------------------------------------------------------------------------
    void clear() {
        my_min = std::numeric_limits<AccountGame::hours_t>::max();
        my_max = std::numeric_limits<AccountGame::hours_t>::min();
        my_accounts.clear();
    }

    AccountGame::hours_t my_min;
    AccountGame::hours_t my_max;

    std::vector<AccountGame> my_accounts;

    // This below isn't technically needed yet, but eventually we'll probably
    // have private variables in the future.
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & my_min;
        ar & my_max;
        ar & my_accounts;
    }

};// end ConsumerTraitBucket;
// BOOST_CLASS_VERSION(ConsumerTraitBucket, 1)

//------------------------------------------------------------------------------
// Based on an initial set of Consumer traits provide a method to search for
// other Consumers who have similar traits as the given ConsumerTraits.
//------------------------------------------------------------------------------
class ConsumerTraitBucketSearch {
public:
    //--------------------------------------------------------------------------
    // Instantiate a ConsumerTraitBucketSearch for a particular game
    //
    // account_games MUST be in ASC order of time played.
    ConsumerTraitBucketSearch(
            uint_fast32_t bucket_size,
            const std::vector<AccountGame> &account_games
    ) : my_target_bucket_size(bucket_size)
    {
        allocate_to_buckets(account_games);
    }

    //--------------------------------------------------------------------------
    // Allow empty initialization which allows reading in from archives.
    ConsumerTraitBucketSearch() : my_target_bucket_size(0) { }

    //--------------------------------------------------------------------------
    ~ConsumerTraitBucketSearch() {
        BOOST_FOREACH(ConsumerTraitBucket *bucket, my_buckets) {
            delete bucket;
        }
        my_buckets.clear();
    }

    //--------------------------------------------------------------------------
    void allocate_to_buckets(const std::vector<AccountGame> &account_games) {
        my_buckets.clear();

        // Assume an even distribution and reserve the memory
        // NOTE: this is likely a premature optimization.
        my_buckets.reserve(account_games.size() / my_target_bucket_size);

        ConsumerTraitBucket *last_bucket = new ConsumerTraitBucket();
        my_buckets.push_back(last_bucket);
        ConsumerTraitBucket *current_bucket = new ConsumerTraitBucket();

        BOOST_FOREACH( AccountGame ag, account_games )
        {
            // If the next account game has changed the bucket bounds
            if (!current_bucket->contains(ag.hours_played)) {

                // See if we can add the current bucket into the old one
                if (last_bucket->size() + current_bucket->size() <= my_target_bucket_size) {
                    // If we can, merge the current bein into the old one
                    // and clear it
                    last_bucket->extend(current_bucket);
                    current_bucket->clear();
                } else {
                    // If we can't. Append the current bucket and set the last bucket to 
                    // the new one.
                    last_bucket = current_bucket;
                    my_buckets.push_back(current_bucket);
                    current_bucket = new ConsumerTraitBucket();
                }
            }

            // At this point the current bucket is the appropriate one to 
            // add this account_game to
            current_bucket->push_back(ag);
        }

        // Add the last one.
        if (current_bucket->size() > 0) {
            my_buckets.push_back(current_bucket);
        }
    }

    //--------------------------------------------------------------------------
    friend std::ostream& operator<< (std::ostream &out, ConsumerTraitBucketSearch &ctbs);

    //--------------------------------------------------------------------------
    // Finds a list of account_ids who have the most similar playtime to the
    // hours_played.  Attempt to keep the matches as close to target_number as
    // possible. Please note that the number of matches returned may very well
    // be larger or smaller than target_number. This is because we will not
    // priotize between accounts that have identical playing time.
    match_heap_t
    consumers_similar_to(
            double hours_played,
            uint_fast32_t target_number
    ) {
        std::vector<AccountGame> matches;
        match_heap_t sorted_matches;
        ConsumerTraitBucket *initial_bucket = NULL;
        unsigned int initial_bucket_i = 0;
        bool found = false;
        for (unsigned int i = 0; i < my_buckets.size(); ++i) {
            initial_bucket = my_buckets[i];
            initial_bucket_i = i;
            if (initial_bucket->contains(hours_played)) {
                found = true;
                break;
            }
        }

        // zero matches!
        if (!found) {
            return sorted_matches;
        }

        // otherwise use these matches and as many nearby as needed to get 
        unsigned int first_bucket_size = initial_bucket->size();
        unsigned int total_needed = 0;
        if (first_bucket_size < target_number) {
            total_needed = target_number - initial_bucket->size();
        }

        BOOST_FOREACH(AccountGame ag, initial_bucket->my_accounts) {
            matches.push_back(ag);
        }
        int lower_i = initial_bucket_i - 1, higher_i = initial_bucket_i + 1;
        int total_buckets = my_buckets.size();
        while (matches.size() < target_number && (lower_i >= 0 && higher_i < total_buckets)) {
            // Get the ones from a bucket lower.
            ConsumerTraitBucket *lower_bucket = my_buckets[lower_i];
            if (total_needed >= lower_bucket->size()) {
                BOOST_FOREACH(AccountGame ag, lower_bucket->my_accounts) {
                    matches.push_back(ag);
                }
                total_needed -= lower_bucket->size();
            }
            lower_i--;

            // Get the matches from the higher bucket.
            ConsumerTraitBucket *higher_bucket = my_buckets[higher_i];
            if (total_needed >= higher_bucket->size()) {
                BOOST_FOREACH(AccountGame ag, higher_bucket->my_accounts) {
                    matches.push_back(ag);
                }
                total_needed -= higher_bucket->size();
            }
            higher_i++;
        }

        // Sort the matches by closeness in playtime
        BOOST_FOREACH(AccountGame matched_game, matches) {
            sorted_matches.push(
                    Match(
                        -std::abs(matched_game.hours_played - hours_played),
                        matched_game.account_id,
                        matched_game.hours_played
                    )
                );
        }

        return sorted_matches;
    }

private:
    //--------------------------------------------------------------------------
    // The target bucket size.
    //
    // Note that not all buckets will actually be this sized - it's just the size
    // we are hoping for.
    uint_fast32_t my_target_bucket_size;

    //--------------------------------------------------------------------------
    std::vector<ConsumerTraitBucket *> my_buckets;

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & my_target_bucket_size;
        ar & my_buckets;
    }

};
// BOOST_CLASS_VERSION(ConsumerTraitBucketSearch, 0)

std::ostream& operator<< (std::ostream &out, ConsumerTraitBucketSearch &ctbs)
{
    out << "My target bucket size: " <<  ctbs.my_target_bucket_size << std::endl;
    out << "Calculated " << ctbs.my_buckets.size() << " buckets." << std::endl;
    uint_fast32_t count = 0;
    BOOST_FOREACH(ConsumerTraitBucket *ab, ctbs.my_buckets) {
        out << "Bin #" << count
            << "[" << ab->my_min << "," << ab->my_max << "]"
            << " has " << ab->size() << " accounts" << std::endl;
        count += 1;
    }
    out << "done";
    return out;
}

//------------------------------------------------------------------------------
class AllGameSearch {
public:
    // TODO: make these auto_ptrs
    typedef std::map<Game::id_t, ConsumerTraitBucketSearch *> search_map_t;
    typedef std::map<uint_fast32_t, double> map_t;

    //--------------------------------------------------------------------------
    // default ctor
    AllGameSearch(uint_fast32_t target_bucket_size)
        : my_target_bucket_size(target_bucket_size)
    { }

    //--------------------------------------------------------------------------
    ~AllGameSearch() {
        BOOST_FOREACH(ConsumerTraitBucketSearch *search, my_products | boost::adaptors::map_values) {
            delete search;
        }
    }

    //--------------------------------------------------------------------------
    void add_game(Game::id_t id, const std::vector<AccountGame> &games) {
        // TODO: Make this atomic??
        my_products[id] = new ConsumerTraitBucketSearch(my_target_bucket_size, games);
    }

    //--------------------------------------------------------------------------
    void add_game(Game::id_t id, ConsumerTraitBucketSearch *ctbs) {
        // TODO: Make this atomic??
        my_products[id] = ctbs;
    }

    //--------------------------------------------------------------------------
    // Remove the game search from this search
    void remove_game(Game::id_t id) {
        // TODO: Make this atomic??
        delete my_products[id];
        my_products.erase(id);
    }

    //--------------------------------------------------------------------------
    // Swap the bucket containing these game records with new game records.
    void swap_game(Game::id_t id, const std::vector<AccountGame> &games) {
        // TODO: Make this atomic!!
        remove_game(id);
        add_game(id, games);
    }


    //--------------------------------------------------------------------------
    // Returns a heap of tuples where each tuple is composed of the priority
    // factor of this match and the second is the account_id
    //
    // NOTE: Assumes games are ordered by playtime descending.
    match_heap_t
    search(const std::vector<AccountGame> &games) {
        double play_time_min = 0.0;
        double play_time_max = 3.0;
        double play_time_range = play_time_max - play_time_min;

        double match_min = 0.5;
        double match_max = 1.5;
        double match_range = match_max - match_min;

        map_t account_id_match_count;
        unsigned int input_game_i = 0;
        double size = (double)games.size();
        BOOST_FOREACH(AccountGame input_game, games) {
            double percent_through_input = (double)input_game_i / size;
            double play_time_factor = play_time_max - (play_time_range * percent_through_input);
            input_game_i++;


            ConsumerTraitBucketSearch *search = my_products[input_game.game_id];
            match_heap_t sorted_matches = 
                search->consumers_similar_to(
                        input_game.hours_played, my_target_bucket_size
                    );
            double size = (double)sorted_matches.size();
            unsigned int per_game_i = 0;
            while (sorted_matches.size() > 0) {
                double percent_through_matches = (double)per_game_i / size;
                double match_factor = match_min + (match_range * percent_through_matches);

                Match result = sorted_matches.top();
                sorted_matches.pop();
                int account_id = result.account_id;

                //std::cout << "Match " << account_id << std::endl;
                account_id_match_count[account_id] += match_factor + play_time_factor;

                per_game_i++;
            }
        }

        match_heap_t matches;
        BOOST_FOREACH(map_t::value_type &account_i, account_id_match_count) {
            matches.push(Match(account_i.second, account_i.first, 0.0));
        }
        return matches;
    }


private:
    search_map_t my_products;
    uint_fast32_t my_target_bucket_size;

};// end AllGameSearch;

}//end namespace steam
}//end namespace gamestradamus
#endif//GAMESTRADAMUS_STEAM_SEARCH_HPP

