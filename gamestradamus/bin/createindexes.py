"""Create a set of indexes locally that can be consumed by the search programs.
"""
import os
import sys
import hydro
from optparse import OptionParser
from gamestradamus import cli, units


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-d", "--directory", dest="directory",
                              help="Write all indexes to this DIRECTORY", metavar="DIRECTORY")
    parser.add_option("-i", action="append", dest="app_ids")
    parser.add_option("-c", "--config", dest="config",
                              help="Use the following configuration file for connection information.")

    (options, args) = parser.parse_args()

    app_ids = options.app_ids
    if app_ids is None:
        store = cli.storage_manager(config=options.config)
        store.map_all(conflicts='error')
        sandbox = store.new_sandbox()
        app_ids = []
        for g in sandbox.xrecall(units.Game, lambda g: g.is_dlc == False and g.canonical_game_id is None):
            app_ids.append(int(g.app_id))
        app_ids = sorted(app_ids)


    db_conf = cli.get_database_configuration(config=options.config)
    for id in app_ids:
        print "Creating index for game %s" % (id,)
        print "./cpp/bin/createindex --bucket_size=1000 --app_id=%s --filename=%s --hostname=%s" % (
                id,
                "%s/game_%s.crystalball" % (options.directory, id,),
                db_conf['host'],
            )
        val = os.system("./cpp/bin/createindex --bucket_size=1000 --app_id=%s --filename=%s --hostname=%s" % (
                id,
                "%s/game_%s.crystalball" % (options.directory, id,),
                db_conf['host'],
            ))
        if val != 0:
            sys.exit()
