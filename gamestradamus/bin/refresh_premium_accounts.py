import random
import smtplib
import datetime
from email.mime.text import MIMEText
from gamestradamus import cli, units, timeutils
from gamestradamus.taskqueue import tasks

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    """Refresh the set of accounts that are premium users.
    """

    config = cli.get_global_config()

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(
        config['gauge.email.server_address'],
        config['gauge.email.password']
    )

    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    premium_cutoff = timeutils.utcnow()
    cut_off = timeutils.utcnow() - datetime.timedelta(hours=4)
    rows, cols = sandbox.store.db.fetch("""
        SELECT "UserAccount".openid
        FROM "UserAccountAttributes"
        INNER JOIN "UserAccount"
            ON "UserAccount".id = "UserAccountAttributes".user_account_id
        WHERE (
                "UserAccountAttributes".games_last_updated < '%s'
                OR "UserAccountAttributes".games_last_updated IS NULL
            )
            AND "UserAccount".openid <> 'Metascore'
            AND "UserAccountAttributes".premium_account_until IS NOT NULL
            AND "UserAccountAttributes".premium_account_until >= '%s'
        ORDER BY
            "UserAccountAttributes".games_last_updated IS NOT NULL ASC,
            "UserAccountAttributes".games_last_updated ASC
        LIMIT 5000
        """ % (cut_off, premium_cutoff)
    )


    # The cronjob will be scheduled for every 2 minutes.
    # we'll schedule enough to process one for every 20s in those two minutes
    # we'll only schedule each user once every 4 hours.
    # This check is to see if we have enough premium users where we can't get through all
    # of them in 4 hours. That should be a good enough warning to use that we should consider
    # changing this script.
    num_premium_users = len(rows)
    four_hour_limit = 5 * 60 * 4
    if num_premium_users > four_hour_limit:
        msg = MIMEText("There are more than %s premium users, can't refresh them all.\r\n\r\n\r\nGauge" % (num_premium_users))
        msg['Subject'] = "Can't refresh all premuim users"
        msg['From'] = config['gauge.email.server_address']
        msg['To'] = config['gauge.email.error_address']

        server.sendmail(
            config['gauge.email.server_address'],
            config['gauge.email.error_address'],
            msg.as_string()
        )

    # We'll do 10 every 2 minutes
    for row in random.sample(rows, 10):
        steam_id_64 = row[0].rsplit('/', 1)[-1]
        result = tasks.steam_update_hours_in_game.apply_async(**{
            'args': [steam_id_64],
            'routing_key': 'tasks.premium.update-games'
        })
    store.shutdown()

